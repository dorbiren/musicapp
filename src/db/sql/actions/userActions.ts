import { IUser } from "../../../types.js";
import bcrypt from "bcryptjs";
import { sql_con } from "../sql.connection.js";
import { OkPacket, RowDataPacket } from "mysql2";
import { generateID } from "../../../utils.js";
import { number } from "yup/lib/locale";


class UserActions {

    arrToUser(arr):IUser|undefined{
        if(arr){

            return{
                id:arr[0],
                email:arr[1],
                password:arr[2],
                token:arr[3],
                role:arr[4]
            }; 
        }
        return undefined;
    }

    async addUser(userDetails:IUser,token:string) {
       const id = generateID();
       const {password} = userDetails;
       const hashed =await bcrypt.hash(password,10);
       const payload = {...userDetails,password:hashed,token,id};
       const query = "INSERT INTO user SET ?";
       await sql_con.query(query,payload);
       const user = await this.getUserById(id);
       return user;
    }

    async getAllUsers() {
        const query = "select * from user";
        const rawData =await sql_con.query(query);
        const users:RowDataPacket = rawData[0] as RowDataPacket;
        
        return users.map(user=>this.arrToUser(user));
    }

    async getUserById (id : string) {
        const query = "SELECT * FROM user WHERE id = ?";
        const rawData = await sql_con.query(query,id);
        const results : RowDataPacket[] = rawData[0] as RowDataPacket[];
        const user = results[0];
        return this.arrToUser(user);
    }


    async getUserByEmail (email : string) {
        const query = "SELECT * FROM user WHERE email = ?";
        const rawData = await sql_con.query(query,email);
        const user : RowDataPacket[] = rawData[0] as RowDataPacket[];
        return this.arrToUser(user);
    }


    async updateUser(userId:string,newDetails:Partial<IUser>) {
        const query = "UPDATE user SET ? WHERE id = ?";
        await sql_con.query(query,[newDetails,userId]);
        const user =await this.getUserById(userId);
        return user;
    }

     deleteUser = async (id : string) =>{
        const user = this.getUserById(id);
        this.deleteAllUserPlaylist(id);
        const query = "DELETE FROM user WHERE id = ?";
        await sql_con.query(query,id);
        
        return user;
    };
   

     async getPageOfUsers(page:number,size:number) {
        const query = "SELECT * FROM user limit  ? offset  ? ";
        const rawData = await sql_con.query(query,[page*size,size]);
        const users:RowDataPacket = rawData[0] as RowDataPacket;

        return users.map(user=>this.arrToUser(user));
     }

    

    async deleteAllUserPlaylist(userId: string) {
        const query = "DELETE FROM playlist WHERE creator = ?";
        await sql_con.query(query,userId);
    }
}
const userActions = new UserActions();
export default userActions;
