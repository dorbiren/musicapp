import { RowDataPacket } from "mysql2";
import { IPlaylist, ISong } from "../../../types.js";
import { generateID } from "../../../utils.js";
import { sql_con } from "../sql.connection.js";
import songActions from "./songActions.js";


class PlaylistActions{


    arrToPlaylist(arr){
        if(arr){

            return{
                id:arr[0],
                name:arr[1],
                creator:arr[2],
                status:arr[3]
            }; 
        }
        return undefined;
    }

async addPlaylist(playlistDetails: Omit<IPlaylist,"id">) {
    const id = generateID();
    const payload = {...playlistDetails,id};
    const sql = "INSERT INTO playlist SET ?";
    await sql_con.query(sql, payload);

    const playlist = await this.getPlaylistById(id);
    return playlist;
}

async getAllPlaylists() {
    const query = "select * from playlist";
    const rawData =await sql_con.query(query);
    const playlists:RowDataPacket = rawData[0] as RowDataPacket;
    
    return playlists.map(playlist=>this.arrToPlaylist(playlist));
}

async getPlaylistById (id : string) {
    const query = "SELECT * FROM playlist WHERE id = ?";
    const rawData = await sql_con.query(query,id);
    const result : RowDataPacket[] = rawData[0] as RowDataPacket[];
    return this.arrToPlaylist(result[0]);
}


    async updatePlaylistDetails(
        playlistId: string,
        newDetails: Partial<IPlaylist>
    ) {
        const query = "UPDATE playlist SET ? WHERE id = ?";
        await sql_con.query(query,[newDetails,playlistId]);
        const playlist =await this.getPlaylistById(playlistId);
        return playlist;
    }

    async deletePlaylist (id : string) {
        const playlist = this.getPlaylistById(id);
        const query = "DELETE FROM playlist WHERE id = ?";
        await sql_con.query(query,id);
        
        return playlist;
    };

    async deletePlaylistSongs(id : string) {
        const playlist = this.getPlaylistById(id);
        const query = "DELETE FROM song_playlist WHERE playlist_id = ?";
        await sql_con.query(query,id);
        
        return playlist;
    };

    async getPlaylistSongs(id : string) {
        const playlist = this.getPlaylistById(id);
        const query = "SELECT * FROM song_playlist WHERE playlist_id = ?";
        const rawdata = await sql_con.query(query,id);
        const songs_playlists = rawdata[0];
        
        return await Promise.all(songs_playlists.map((sp)=> songActions.getSongById(sp[1])))
    };

    async addSongToPlaylist(playlist_id:string,song_id:string){
        const id = generateID();
        const query = "INSERT INTO song_playlist SET ?";
        await sql_con.query(query,{id,song_id,playlist_id});
    }

    async removeSongFromPlaylist(playlistId:string,songId:string) {
        const query = "DELETE FROM song_playlist WHERE playlist_id = ? AND song_id = ?"
        await sql_con.query(query,[playlistId,songId]);
    }

    async removeSongFromAllPlaylists(songId:string) {
        const query = "DELETE FROM song_playlist WHERE song_id = ?";
        await sql_con.query(query,songId);
    }

    


    

}


const playlistActions = new PlaylistActions();
export default playlistActions;
