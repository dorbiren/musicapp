import { IArtist, ISong } from "../../../types.js";
import pkg from "bluebird";
import {generateID} from "../../../utils.js";
import { sql_con } from "../sql.connection.js";
import { RowDataPacket } from "mysql2";
import songActions from "./songActions.js";
const { Promise } = pkg;

class ArtistActions {

    arrToArtist(arr){
        if(arr){

            return{
                id:arr[0],
                name:arr[1],
                creator:arr[2],
                status:arr[3]
            }; 
        }
        return undefined;
    }
    
    async addArtist(artistDetails: Omit<IArtist,"id">) {
        const id = generateID();
        const payload = {...artistDetails,id};
        const sql = "INSERT INTO artist SET ?";
        await sql_con.query(sql, payload);

        const artist = await this.getArtistById(id);
        return artist;
    }

    async getAllArtists() {
        const query = "select * from artist";
        const rawData =await sql_con.query(query);
        const artists:RowDataPacket = rawData[0] as RowDataPacket;
        
        return artists.map(artist=>this.arrToArtist(artist));
    }

    async getArtistById (id : string) {
        const query = "SELECT * FROM artist WHERE id = ?";
        const rawData = await sql_con.query(query,id);
        const result : RowDataPacket[] = rawData[0] as RowDataPacket[];
        return this.arrToArtist(result[0]);
    }


        async updateArtist(
            artistId: string,
            newDetails: Partial<IArtist>
        ) {
            const query = "UPDATE artist SET ? WHERE id = ?";
            await sql_con.query(query,[newDetails,artistId]);
            const artist =await this.getArtistById(artistId);
            return artist;
        }

        deleteArtist = async (id : string) =>{
            const artist = this.getArtistById(id);
            const query = "DELETE FROM artist WHERE id = ?";
            await this.deleteAllArtistSongs(id);
            await sql_con.query(query,id);
            
            return artist;
        };
        

        async getAllArtistSong(artistId: string) {
            const query = "select * FROM song WHERE artist = ?";
            const rawData = await sql_con.query(query,artistId);
            const songs:RowDataPacket[] = rawData[0] as RowDataPacket;
            return songs.map(song=>songActions.arrToSong(song));
        }

        async deleteAllArtistSongs(artistId: string) {
            const query = `DELETE FROM song_playlist
             WHERE song_id in 
             (SELECT id as song_id FROM song WHERE artist = ?)`;
            await sql_con.query(query,artistId);

            const query2 = "DELETE FROM song WHERE artist = ?";
            await sql_con.query(query2,artistId);

        }

        
}

const artistActions = new ArtistActions();
export default artistActions;
