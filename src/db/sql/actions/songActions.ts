import { RowDataPacket } from "mysql2";
import { ISong } from "../../../types.js";
import { generateID } from "../../../utils.js";
import { sql_con } from "../sql.connection.js";
import  playlistActions  from "./playlistActions.js";


class SongActions {
    
     arrToSong(arr){
        if(arr){
            return{
                id:arr[0],
                song_name:arr[1],
                artist:arr[2],
                status:arr[3],
                creator:arr[4],
            }; 
        }
        return undefined;
    }

//     async addSong(songDetails: Omit<ISong, "id">) {
//         const { artist } = songDetails;
//         const owner = await artistModel.findById(artist);
//         if (!owner) {
//             throw new Error("artist not exist!");
//         }
//         const song = await song_model.create(songDetails);
//         const songId = song.id;
    
//         owner.songs.push(songId);
//         owner.save();
    
//         return song;
//     }

async addSong(songDetails: Omit<ISong,"id">) {
    const id = generateID();
    const payload = {...songDetails,id};
    const sql = "INSERT INTO song SET ?";
    await sql_con.query(sql, payload);

    const song = await this.getSongById(id);
    return song;
}

async getAllSongs() {
    const query = "select * from song";
    const rawData =await sql_con.query(query);
    const songs:RowDataPacket = rawData[0] as RowDataPacket;
    
    return songs.map(song=>this.arrToSong(song));
}

async getSongById (id : string) {
    const query = "SELECT * FROM song WHERE id = ?";
    const rawData = await sql_con.query(query,id);
    const result : RowDataPacket[] = rawData[0] as RowDataPacket[];
    return this.arrToSong(result[0]);
}

async updateSong(
    songId: string,
    newDetails: Partial<Isong>
) {
    const query = "UPDATE song SET ? WHERE id = ?";
    await sql_con.query(query,[newDetails,songId]);
    const song =await this.getSongById(songId);
    return song;
}

deleteSong = async (id : string) =>{
    const song = this.getSongById(id);
    await playlistActions.removeSongFromAllPlaylists(id);
    const query = "DELETE FROM song WHERE id = ?";
    const result = await sql_con.query(query,id);
    
    return song;
};

}

const songActions = new SongActions();
export default songActions;


