import mongoose from "mongoose";
import log from "@ajar/marker";

export const connect_db = async (uri: string) => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    };
    await mongoose.connect(uri, options as mongoose.ConnectOptions);
    log.magenta(" ✨  Connected to Mongo DB ✨ ");
};
