import playlist_model from "../models/playlist.model.js";
import { IPlaylist, ISong } from "../../../types.js";
import songModel from "../models/song.model.js";


class PlaylistActions{


async addPlaylist(playlistDetails:Omit<IPlaylist,"id">) {
    const playlist = await playlist_model.create(playlistDetails);
    return playlist;
}
async getAllPlaylists() {
    const playlists = await playlist_model.find();
    return playlists;
}
async getPlaylistById(playlistId:string) {
    const playlist = await playlist_model.findById(playlistId);
    return playlist;
}
async updatePlaylistDetails(playlistId:string,newDetails:Partial<IPlaylist>) {
    const playlist = await playlist_model.findByIdAndUpdate(playlistId,newDetails,{new:true,upsert:false});
    return playlist;
}
async addSongToPlaylist(playlistId:string,songId:string) {
    const playlist = await playlist_model.findById(playlistId);
    const song = await songModel.findById(songId);
    playlist.songs.push(song);
    await playlist.save();
    return playlist;
}
async removeSongFromPlaylist(playlistId:string,songId:string) {
    const playlist = await playlist_model.findById(playlistId);
    playlist.songs = playlist.songs.filter((s:ISong) =>songId !== s.id);
    await playlist.save();
    return playlist;
}
async deletePlaylist(playlistId:string) {
    const playlist = await playlist_model.findByIdAndRemove(playlistId);
    return playlist;
}

}


const playlistActions = new PlaylistActions();
export default playlistActions;
