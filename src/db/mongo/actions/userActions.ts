import user_model from "../models/user.model.js";
import { IUser } from "../../../types.js";
import bcrypt from "bcryptjs";

class UserActions{

    async addUser(userDetails:IUser,token:string) {
       const {password} = userDetails;
       const hashed =await bcrypt.hash(password,10);
       const {id,first_name,last_name,email} = await user_model.create({...userDetails,password:hashed,token});
       return {id,first_name,last_name,email} ;
    }
    
    async getAllUsers() {
       const users = await user_model.find();
       if(users){
         return users;
      }
      throw new Error("cant find user...");
    }
    
    async getUserById(userId:string) {
       const user = await user_model.findById(userId);
       if(user){
         return user;
      }
      throw new Error("cant find user...")
    
    }

    async getUserByEmail(email:string){
      const user = await user_model.findOne({email});
      if(user){
         return user;
      }
      throw new Error("cant find user...");
    }
    
    async updateUser(userId:string,newDetails:Partial<IUser>) {
       const user = await user_model.findByIdAndUpdate(userId,newDetails,{new:true,upsert:false});
       if(user){
         return user;
      }
      throw new Error("cant find user...");
    }
    
    async deleteUser(userId:string,) {
       const user = await user_model.findByIdAndRemove(userId);
       if(user){
         return user;
      }
      throw new Error("cant find user...");
    }
    
    async getPageOfUsers(page:number,size:number) {
       const users = await user_model
               .find()
               .skip(page * size)
               .limit(size);
       return users;
    }
}
const userActions = new UserActions();
export default userActions;


