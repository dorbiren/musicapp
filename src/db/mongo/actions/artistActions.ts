import artist_model from "../models/artist.model.js";
import { IArtist, ISong } from "../../../types.js";
import songActions from "./songActions.js";
import pkg from "bluebird";
const { Promise } = pkg;

class ArtistActions{

    
    async addArtist(artistDetails: Omit<IArtist, "id">) {
        const artist = await artist_model.create(artistDetails);
        return artist;
    }
    
    async getAllArtists() {
        const artists = await artist_model.find();
        return artists;
    }
    
    async getArtistById(artistId: string) {
        const artist = await artist_model.findById(artistId);
        return artist;
    }
    
    async updateArtist(
        artistId: string,
        newDetails: Partial<IArtist>
    ) {
        const artist = await artist_model.findByIdAndUpdate(artistId, newDetails, {
            new: true,
            upsert: false,
        });
        return artist;
    }
    
    async deleteArtist(artistId: string) {
        const artist: IArtist = await artist_model.findById(artistId);
        const songsIdToDelete = artist.songs;
        await Promise.each(songsIdToDelete, (id) => songActions.deleteSong(id));
        const removed = await artist_model.findByIdAndRemove(artistId);
        return removed;
    }
    
    async deleteSongFromArtist(artistId: string, songId: string) {
        const artist = await artist_model.findById(artistId);
        artist.songs = artist.songs.filter(
            (s: ISong) => songId.toString() !== s.id.toString()
        );
        await artist.save();
        return artist;
    }
    
    async getAllArtistSong(artistId: string) {
        const artist = await artist_model.findById(artistId).populate("songs");
        const songs = artist.songs;
        return songs;
    }
}



const mongo = new ArtistActions();
export default mongo;
