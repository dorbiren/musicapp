import song_model from "../models/song.model.js";
import { ISong } from "../../../types.js";
import artistModel from "../models/artist.model.js";
import  artistActions  from "./artistActions.js";


class SongActions {

    async addSong(songDetails: Omit<ISong, "id">) {
        const { artist } = songDetails;
        const owner = await artistModel.findById(artist);
        if (!owner) {
            throw new Error("artist not exist!");
        }
        const song = await song_model.create(songDetails);
        const songId = song.id;
    
        owner.songs.push(songId);
        owner.save();
    
        return song;
    }
    
    async getAllSongs() {
        const songs = await song_model.find();
        return songs;
    }
    
    async getSongById(songId: string) {
        const song = await song_model.findById(songId);
        return song;
    }
    
    async updateSong(songId: string, newDetails: Partial<ISong>) {
        const song = await song_model.findByIdAndUpdate(songId, newDetails, {
            new: true,
            upsert: false,
        });
        return song;
    }
    
    async deleteSong(songId: string) {
        const song = await song_model.findById(songId);
        artistActions.deleteSongFromArtist(song.artist, song.id);
        const deleted = await song_model.findByIdAndRemove(songId);
        return deleted;
    }

}

const songActions = new SongActions();
export default songActions;


