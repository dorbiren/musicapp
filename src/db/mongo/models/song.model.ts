import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const SongSchema = new Schema({
    song_name: {
        type: String,
        required: true,
        minlength: [2, "name is to short"],
    }, 
    album: {
        type: String,
        required: true,
    },
    lyrics:{ type: String},
    artist:{type: Schema.Types.ObjectId, ref:"artist",required:true}
});

export default model("song", SongSchema);
