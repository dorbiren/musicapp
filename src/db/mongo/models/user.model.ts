import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name: {
        type: String,
        required: true,
        minlength: [2, "name is to short"],
    },
    last_name: {
        type: String,
        required: true,
        minlength: [2, "lastname is to short"],
    },
    email: {
        type: String,
        required: true,
        unique: true,
        match: [
            /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
            "email address not valid",
        ],
    },
    password:{
        required:true,
        minlength:[5,"password has to be longer then 5 letters"],
        type:String
    },
    token:{type:String ,required:true}
});

export default model("user", UserSchema);
