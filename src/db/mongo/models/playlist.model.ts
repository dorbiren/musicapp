import mongoose from "mongoose";
import {SongSchema} from "./song.model.js";
const { Schema, model } = mongoose;

export const PlaylistSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [2, "name is to short"],
    },
    songs:[SongSchema]
});

export default model("playlist", PlaylistSchema);
