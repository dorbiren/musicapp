import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const ArtistSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [2, "name is to short"],
    },
    age: { type: Number },
    nationality: {type:String},
    songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
});

export default model("artist", ArtistSchema);
