//import songActions from "../db/mongo/actions/songActions.js";
import songActions from "../db/sql/actions/songActions.js";
import { ISong } from "../types.js";

export async function addSong(songDetails: Omit<ISong, "id">) {
    const song = songActions.addSong({...songDetails,status:1});
    return song;
}

export async function getAllSongs() {
    const songs = await songActions.getAllSongs();
    return songs;
}

export async function getSongById(songId: string) {
    const song = await songActions.getSongById(songId);
    return song;
}

export async function updateSong(songId: string, newDetails: Partial<ISong>) {
    const song = await songActions.updateSong(songId, newDetails);
    return song;
}

export async function deleteSong(songId: string) {
    const deleted = await songActions.deleteSong(songId);
    return deleted;
}
