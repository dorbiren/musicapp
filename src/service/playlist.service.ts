import playlistActions from "../db/sql/actions/playlistActions.js";
//import playlistActions from "../db/mongo/actions/playlistActions.js";
import { IPlaylist } from "../types.js";

export async function addPlaylist(playlistDetails: Omit<IPlaylist, "id">) {
    const playlist = await playlistActions.addPlaylist({...playlistDetails});
    return playlist;
}
export async function getAllPlaylists() {
    const playlists = await playlistActions.getAllPlaylists();
    return playlists;
}
export async function getPlaylistById(playlistId: string) {
    const playlist = await playlistActions.getPlaylistById(playlistId);
    return playlist;
}
export async function updatePlaylistDetails(
    playlistId: string,
    newDetails: Partial<IPlaylist>
) {
    const playlist = await playlistActions.updatePlaylistDetails(
        playlistId,
        newDetails
    );
    return playlist;
}
export async function addSongToPlaylist(playlistId: string, songId: string) {
    const playlist = playlistActions.addSongToPlaylist(playlistId, songId);
    return playlist;
}
export async function removeSongFromPlaylist(
    playlistId: string,
    songId: string
) {
    const playlist = await playlistActions.removeSongFromPlaylist(
        playlistId,
        songId
    );
    return playlist;
}
export async function deletePlaylist(playlistId: string) {
    const playlist = await playlistActions.deletePlaylist(playlistId);
    return playlist;
}

export async function getPlaylistSongs(playlistId: string) {
    const playlist = await playlistActions.getPlaylistSongs(playlistId);
    return playlist;
}
