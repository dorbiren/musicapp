import { IUser } from "../types.js";
// import UserActions from "../db/mongo/actions/userActions.js";
import UserActions from "../db/sql/actions/userActions.js";

import { createTokens } from "../utils.js";
import jwt from "jsonwebtoken";
import userActions from "../db/mongo/actions/userActions.js";
import bcrypt from "bcryptjs";
import { use } from "chai";

const { APP_SECRET = "", ACCESS_TOKEN_EXPIRATION } = process.env;

export async function addUser(userDetails: IUser) {
    const { id } = await UserActions.addUser({...userDetails,role:"1"}, "0") as IUser; //save to db
    const { access_token, refresh_token } = createTokens({
        ...userDetails,
        role:"1",
        id,
    });
    const user = await updateUser(id, { token: refresh_token });
    return { user, access_token, refresh_token };
}

export async function getAllUsers() {
    const users = await UserActions.getAllUsers();
    return users;
}

export async function getUserById(userId: string) {
    const user = await UserActions.getUserById(userId);
    return user;
}

export async function updateUser(userId: string, newDetails: Partial<IUser>) {
    const user = await UserActions.updateUser(userId, newDetails);
    delete user?.token
    delete user?.password
    return user;
}

export async function deleteUser(userId: string) {
    const user = await UserActions.deleteUser(userId);
    return user;
}

export async function getPageOfUsers(page: number, size: number) {
    const users = await UserActions.getPageOfUsers(page, size);
    return users;
}

export async function getAccessToken(refresh_token: string) {
    const decoded = await jwt.verify(refresh_token, APP_SECRET);
    const { id, profile } = decoded;

    const user = await UserActions.getUserById(id);
    const {role} = user
    const DB_Token = user.token;

    if (DB_Token === refresh_token) {
        const access_token = jwt.sign({ id, role }, APP_SECRET, {
            expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
        });
        return access_token;
    }
}

export async function login(email: string, password: string) {
    const user = await userActions.getUserByEmail(email);
    const match = await bcrypt.compare(password, user.password);

    if (match && user) {
        const { access_token, refresh_token } = createTokens(user);
        await userActions.updateUser(user.id, { token: refresh_token });
        return { access_token, refresh_token };
    } else {
        throw new Error("email or password is invalid");
    }
}
