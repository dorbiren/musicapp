import artistActions from "../db/sql/actions/artistActions.js";
// import artistActions from `../db/mongo/actions/artistActions.js`;
import { IArtist } from "../types.js";

export async function addArtist(artistDetails: Omit<IArtist, "id">) {
    const artist = await artistActions.addArtist({...artistDetails,status:1});
    return artist;
}

export async function getAllArtists() {
    const artists = await artistActions.getAllArtists();
    return artists;
}

export async function getArtistById(artistId: string) {
    const artist = await artistActions.getArtistById(artistId);
    return artist;
}

export async function updateArtist(
    artistId: string,
    newDetails: Partial<IArtist>
) {
    const artist = await artistActions.updateArtist(artistId, newDetails);
    return artist;
}

export async function deleteArtist(artistId: string) {
    const artist: IArtist = await artistActions.deleteArtist(artistId);
    return artist;
}

export async function deleteSongFromArtist(artistId: string, songId: string) {
    const artist = await artistActions.deleteSongFromArtist(artistId, songId);
    return artist;
}

export async function getAllArtistSong(artistId: string) {
    const songs = await artistActions.getAllArtistSong(artistId);
    return songs;
}


