import jwt from "jsonwebtoken";
import { IUser } from "./types.js";
import uuid  from "uuid";

const { ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION, APP_SECRET } =
    process.env;

export function createTokens(user: IUser) {
    const {id,role} = user;
    const access_token = jwt.sign({ id,role }, APP_SECRET as string, {
        expiresIn: ACCESS_TOKEN_EXPIRATION, // expires in 1 minute
    });

    const refresh_token = jwt.sign(
        { id, profile: JSON.stringify(user) },
        APP_SECRET as string,
        {
            expiresIn: REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
        }
    );

    return {
        access_token,
        refresh_token,
    };
}

export function generateID() {
    // return Math.random().toString(32).slice(2);
    return uuid.v4();
}
export function getTimeString() {
    const date = new Date();
    return `${date.toDateString()} ${date.toTimeString()}`;
}
