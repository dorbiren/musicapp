import raw from "../middleware/route.async.wrapper.js";
import { NextFunction, Response, Request } from "express";
import jwt from "jsonwebtoken";

const { APP_SECRET = "" } = process.env;

export const auth = raw(
    async (req: Request, res: Response, next: NextFunction) => {
        const token = req.headers["x-access-token"];

        if (!token)
            return res.status(403).json({
                status: "Unauthorized",
                payload: "No token provided.",
            });

        // verifies secret and checks exp
        const decoded = await jwt.verify(token as string, APP_SECRET);

        // if everything is good, save to request for use in other routes
        req.id = decoded.id;

        next();
    }
);


