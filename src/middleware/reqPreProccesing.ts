import { generateID, getTimeString } from "../utils.js";
import fs from "fs/promises";
import { RequestHandler } from "express";

const LOGGERPATH = `${process.cwd()}/requests_log.txt`;

export const addIdToReq: RequestHandler = (req, res, next) => {
    req.id = generateID();
    next();
};
export const logRequest: RequestHandler = (req, res, next) => {
    fs.writeFile(
        LOGGERPATH,
        `${req.method} , ${req.originalUrl} , ${
            req.id
        } -- ${getTimeString()}\n`,
        {
            flag: "a",
        }
    );
    next();
};
