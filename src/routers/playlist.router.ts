import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import {
    addPlaylist,
    getAllPlaylists,
    getPlaylistById,
    updatePlaylistDetails,
    addSongToPlaylist,
    removeSongFromPlaylist,
    deletePlaylist,
    getPlaylistSongs,
} from "../service/playlist.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW PLAYLIST;
router.post(
    "/",
    raw(async (req: Request, res: Response) => {
        const song = await addPlaylist(req.body);
        res.status(200).json(song);
    })
);

// GET ALL PLAYLISTS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const songs = await getAllPlaylists();
        res.status(200).json(songs);
    })
);

// GETS A SINGLE PLAYLIST
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await getPlaylistById(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);
// UPDATES A SINGLE PLAYLIST
router.put(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await updatePlaylistDetails(req.params.id, req.body);
        res.status(200).json(song);
    })
);

// DELETES A PLAYLIST
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await deletePlaylist(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);

// ADD A SONG TO PLAYLIST
router.put(
    "/add/:id",
    raw(async (req: Request, res: Response) => {
        const song = await addSongToPlaylist(req.params.id, req.body.songId);
        res.status(200).json(song);
    })
);

//remove playlist
router.put(
    "/remove/:id",
    raw(async (req: Request, res: Response) => {
        const song = await removeSongFromPlaylist(
            req.params.id,
            req.body.songId
        );
        res.status(200).json(song);
    })
);
router.get(
    "/songs/:id",
    raw(async (req: Request, res: Response) => {
        const songs = await getPlaylistSongs(req.params.id);
        res.status(200).json(songs);
    })
);

export default router;
