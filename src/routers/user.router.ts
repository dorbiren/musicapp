import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import { validateUser } from "../middleware/uservalidator.js";
import { Request, Response } from "express";
import ms from "ms";
import {
    addUser,
    getAllUsers,
    getUserById,
    updateUser,
    deleteUser,
    getPageOfUsers,
    getAccessToken,
    login,
} from "../service/user.service.js";

import cookieParser from "cookie-parser";

//const { APP_SECRET = "" } = process.env;

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());
router.use(cookieParser());

// REGISTER A NEW USER;
router.post(
    "/",
    validateUser,
    raw(async (req: Request, res: Response) => {
        const { user, access_token, refresh_token } = await addUser(req.user);

        res.cookie("refresh_token", refresh_token, {
            maxAge: ms("60d"), //60 days
            httpOnly: true,
        });
        res.status(200).json({ user, access_token });
    })
);
//get-access-token
router.get(
    "/get-access-token",
    raw(async (req, res) => {
        //get refresh_token from client - req.cookies
        const { refresh_token } = req.cookies;

        if (!refresh_token) {
            throw new Error("Unauthorized - Failed to verify refresh_token.");
        }

        const access_token = await getAccessToken(refresh_token);
        if (access_token) {
            res.status(200).json({ access_token });
        } else {
            throw new Error("Unauthorized - Failed to verify refresh_token.");
        }
    })
);

// GET ALL USERS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const users = await getAllUsers();
        res.status(200).json(users);
    })
);

// GETS A SINGLE USER
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await getUserById(req.params.id);
        // .select(`-id
        //     first_name
        //     last_name
        //     email
        //     phone`);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);
// UPDATES A SINGLE USER
router.put(
    "/:id",
    //validateUser,
    raw(async (req: Request, res: Response) => {
        const user = await updateUser(req.params.id, req.body);
        res.status(200).json(user);
    })
);

// DELETES A USER
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await deleteUser(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);
// get page of USERs

router.get(
    "/pagination/:page/:size",
    raw(async (req: Request, res: Response) => {
        const page = Number(req.params.page);
        const size = Number(req.params.size);

        if (isNaN(page) || isNaN(size)) {
            throw new Error("page and size should be numbers");
        }
        const users = await getPageOfUsers(page, size);
        res.json(users);
    })
);
//login
router.post(
    "/login",
    raw(async (req: Request, res: Response) => {
        const { email, password } = req.body;
        const { access_token, refresh_token } = await login(email, password);

        res.cookie("refresh_token", refresh_token, {
            maxAge: ms("60d"), //60 days
            httpOnly: true,
        });

        res.status(200).json(access_token);
    })
);

export default router;

// const access_token = jwt.sign({ id : req.user.id , some:'other value'}, APP_SECRET, {
//     expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute
//   })
//   const refresh_token = jwt.sign({ id : req.user.id , profile:JSON.stringify(user)}, APP_SECRET, {
//     expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term...
//   })

//   res.cookie('refresh_token',refresh_token, {
//     maxAge: ms('60d'), //60 days
//     httpOnly: true
//   })
//   res.redirect(`${CLIENT_ORIGIN}?token=${access_token}&profile=${encodeURIComponent(JSON.stringify(user))}`)
