import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import {
    addArtist,
    getAllArtists,
    getArtistById,
    updateArtist,
    deleteArtist,
    getAllArtistSong,
} from "../service/artist.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW ARTIST
router.post(
    "/",
    raw(async (req: Request, res: Response) => {
        const song = await addArtist(req.body);
        res.status(200).json(song);
    })
);
// GET ALL ARTISTS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const songs = await getAllArtists();
        res.status(200).json(songs);
    })
);
// GETS A SINGLE ARTIST
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await getArtistById(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);
// UPDATES A SINGLE ARTIST
router.put(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await updateArtist(req.params.id, req.body);
        res.status(200).json(song);
    })
);

// DELETES A ARTIST
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await deleteArtist(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);

// GET ALL SONGS BY ARTIST ID
router.get(
    "/allsongs/:id",
    raw(async (req: Request, res: Response) => {
        const songs = await getAllArtistSong(req.params.id);
        if (!songs) return res.status(404).json({ status: "No songs found." });
        res.status(200).json(songs);
    })
);

export default router;
