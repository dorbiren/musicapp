import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import {
    addSong,
    getAllSongs,
    getSongById,
    updateSong,
    deleteSong,
} from "../service/song.service.js";
import { auth } from "../middleware/auth.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());
//router.use(auth);

// CREATES A NEW SONG;
router.post(
    "/",
    raw(async (req: Request, res: Response) => {
        const song = await addSong(req.body);
        res.status(200).json(song);
    })
);

// GET ALL SONGS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const songs = await getAllSongs();
        res.status(200).json(songs);
    })
);

// GETS A SINGLE SONG
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await getSongById(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await getSongById(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);
// UPDATES A SINGLE SONG
router.put(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await updateSong(req.params.id, req.body);
        res.status(200).json(song);
    })
);

// DELETES A SONG
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await deleteSong(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);

export default router;
